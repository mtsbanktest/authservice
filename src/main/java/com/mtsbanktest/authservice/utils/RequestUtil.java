package com.mtsbanktest.authservice.utils;

import com.mtsbanktest.authservice.exception.InvalidPhoneFormatException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class RequestUtil {

    public static void validatePhoneNumberFormat(String phone) {
        if (StringUtils.length(phone) < 10) {
            log.error("Incorrect phone number format: {}.", phone);
            throw new InvalidPhoneFormatException();
        }
    }


}
