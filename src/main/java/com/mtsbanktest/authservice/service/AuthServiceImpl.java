package com.mtsbanktest.authservice.service;

import com.mtsbanktest.authservice.dao.entity.UserEntity;
import com.mtsbanktest.authservice.dao.repository.UserRepository;
import com.mtsbanktest.authservice.exception.IncorrectCodeException;
import com.mtsbanktest.authservice.exception.ParseTokenException;
import com.mtsbanktest.authservice.exception.UserNotFoundException;
import com.mtsbanktest.authservice.kafka.message.SuccessfullAuthMessage;
import com.mtsbanktest.authservice.kafka.producer.AuthentificationTopicSender;
import com.mtsbanktest.authservice.model.request.AuthRequest;
import com.mtsbanktest.authservice.model.response.AuthResponse;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {
    private final UserRepository userRepository;
    private final JwtProvider jwtProvider;
    private final AuthentificationTopicSender authentificationTopicSender;
    private final Map<String, String> refreshStorage = new HashMap<>();

    @Override
    @Transactional
    public AuthResponse authenticateUser(AuthRequest authRequest) {

        checkValidCode(authRequest.getCode());
        var maybeUser = userRepository.findUserByPhone(authRequest.getPhone());
        if (maybeUser.isPresent()) {
            maybeUser.get().setUpdatedDate(LocalDateTime.now());
            userRepository.updateDate(maybeUser.get().getPhone(), maybeUser.get().getUpdatedDate());
        } else {
           maybeUser = registerNewUser(authRequest.getPhone());
        }

        final String accessToken = jwtProvider.generateAccessToken(maybeUser.get());
        final String refreshToken = jwtProvider.generateRefreshToken(maybeUser.get());
        authentificationTopicSender.sendMessage(new SuccessfullAuthMessage(authRequest.getPhone(), "Пользователь авторизовался в наш сервис"));
        return new AuthResponse(accessToken, refreshToken);
    }

    @Override
    public AuthResponse getAccessToken(String refreshToken) {
        if (jwtProvider.validateRefreshToken(refreshToken)) {
            final Claims claims = jwtProvider.getRefreshClaims(refreshToken);
            final String phone = claims.getSubject();
            final String saveRefreshToken = refreshStorage.get(phone);
            if (saveRefreshToken != null && saveRefreshToken.equals(refreshToken)) {
                final UserEntity user = userRepository.findUserByPhone(phone)
                        .orElseThrow(UserNotFoundException::new);
                final String accessToken = jwtProvider.generateAccessToken(user);
                return new AuthResponse(accessToken, null);
            }
        }
        return new AuthResponse(null, null);
    }

    @Override
    public AuthResponse refreshToken(String refreshToken) {

        if (jwtProvider.validateRefreshToken(refreshToken)) {
            final Claims claims = jwtProvider.getRefreshClaims(refreshToken);
            final String login = claims.getSubject();
            final String saveRefreshToken = refreshStorage.get(login);
            if (saveRefreshToken != null && saveRefreshToken.equals(refreshToken)) {
                final UserEntity user = userRepository.findUserByPhone(login)
                        .orElseThrow(UserNotFoundException::new);
                final String accessToken = jwtProvider.generateAccessToken(user);
                final String newRefreshToken = jwtProvider.generateRefreshToken(user);
                refreshStorage.put(user.getPhone(), newRefreshToken);
                return new AuthResponse(accessToken, newRefreshToken);
            }
        }
        throw new ParseTokenException();
    }


    private Optional<UserEntity> registerNewUser(String phone) {
        UserEntity user = userRepository.save(UserEntity
                .builder()
                .phone(phone)
                .createdDate(LocalDateTime.now())
                .updatedDate(LocalDateTime.now())
                .build());
        return Optional.ofNullable(user);
    }

    private void checkValidCode(String code) {
        String [] codes = {"1234", "5343", "3421"};
        if (!Arrays.asList(codes).contains(code))
            throw new IncorrectCodeException();
    }
}
