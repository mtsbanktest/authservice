package com.mtsbanktest.authservice.service;

import com.mtsbanktest.authservice.model.request.AuthRequest;
import com.mtsbanktest.authservice.model.response.AuthResponse;

public interface AuthService {


    AuthResponse authenticateUser(AuthRequest authRequest);
    AuthResponse getAccessToken(String refreshToken);
    AuthResponse refreshToken(String refreshToken);
}
