package com.mtsbanktest.authservice.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity {

    private long userId;
    private long walletId;
    private String phone;
    private LocalDateTime createdDate;
    private LocalDateTime updatedDate;


}
