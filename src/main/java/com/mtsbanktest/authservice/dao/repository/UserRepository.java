package com.mtsbanktest.authservice.dao.repository;

import com.mtsbanktest.authservice.dao.entity.UserEntity;

import java.time.LocalDateTime;
import java.util.Optional;

public interface UserRepository {

    UserEntity save(UserEntity userEntity);

    int updateDate(String phone, LocalDateTime updatedDate);

    Optional<UserEntity> findUserByPhone(String username);


}
