package com.mtsbanktest.authservice.dao.repository.impl;

import com.mtsbanktest.authservice.dao.entity.UserEntity;
import com.mtsbanktest.authservice.dao.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
@Slf4j
public class UserRepositoryImpl implements UserRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private static final String INSERT_NEW_USER = "INSERT INTO user_registrations (phone, created_date, updated_date) " +
            "VALUES (:phone,:createdDate,:updatedDate)";
    private static final String SELECT_CURRENT_USER_ID = "SELECT currval(pg_get_serial_sequence('user_registrations', 'user_id'))";
    private static final String SELECT_CURRENT_WALLET_ID = "SELECT currval(pg_get_serial_sequence('user_registrations', 'wallet_id'))";
    private static final String SELECT_USER_BY_PHONE = "SELECT * " +
            "FROM user_registrations " +
            "WHERE phone = :phone";

    private static final String UPDATE_DATE = "UPDATE user_registrations " +
            "SET updated_date = :updatedDate " +
            "WHERE phone = :phone";

    @Override
    public UserEntity save(UserEntity userEntity) {
        log.debug("Вызван метод save UserRepositoryImpl с аргументом {}", userEntity);
        jdbcTemplate.update(INSERT_NEW_USER, Map.of(
                "phone", userEntity.getPhone(),
                "createdDate", userEntity.getCreatedDate(),
                "updatedDate", userEntity.getUpdatedDate()));
        var userId = jdbcTemplate.queryForObject(SELECT_CURRENT_USER_ID, Map.of(), (rs, rn) -> rs.getLong(1));
        var walletId = jdbcTemplate.queryForObject(SELECT_CURRENT_WALLET_ID, Map.of(), (rs, rn) -> rs.getLong(1));
        return new UserEntity(userId, walletId, userEntity.getPhone(), LocalDateTime.now(), LocalDateTime.now());
    }

    @Override
    public int updateDate(String phone, LocalDateTime updatedDate) {
        return jdbcTemplate.update(UPDATE_DATE, Map.of("updatedDate", updatedDate, "phone", phone));
    }

    @Override
    public Optional<UserEntity> findUserByPhone(String phone) {
       try {
           return Optional.of(jdbcTemplate.queryForObject(SELECT_USER_BY_PHONE, Map.of("phone", phone),
                   (rs, rn) ->
                           new UserEntity(rs.getLong("user_id"),
                                   rs.getLong("wallet_id"),
                                   rs.getString("phone"),
                                   rs.getTimestamp("created_date").toLocalDateTime(),
                                   rs.getTimestamp("updated_date").toLocalDateTime())

           ));
       } catch (EmptyResultDataAccessException e) {
           return Optional.empty();
       }

    }
}
