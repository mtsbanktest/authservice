package com.mtsbanktest.authservice.kafka.producer.impl;

import com.mtsbanktest.authservice.config.ProducerProperties;
import com.mtsbanktest.authservice.kafka.message.SuccessfullAuthMessage;
import com.mtsbanktest.authservice.kafka.producer.AuthentificationTopicSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class AuthentificationTopicSenderImpl implements AuthentificationTopicSender {

    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final ProducerProperties producerProperties;

    @Override
    public void sendMessage(SuccessfullAuthMessage successfullAuthMessage) {
        try {
            kafkaTemplate.send(producerProperties.getTopic(), successfullAuthMessage);
        } catch (Exception e) {
            log.warn("Ошибка при отправке сообщения в топик , {}", e.getMessage());
        }
    }
}
