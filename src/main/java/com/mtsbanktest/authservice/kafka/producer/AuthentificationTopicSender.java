package com.mtsbanktest.authservice.kafka.producer;

import com.mtsbanktest.authservice.kafka.message.SuccessfullAuthMessage;

public interface AuthentificationTopicSender {

    void sendMessage(SuccessfullAuthMessage successfullAuthMessage);

}
