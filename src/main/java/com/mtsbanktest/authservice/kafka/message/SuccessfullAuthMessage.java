package com.mtsbanktest.authservice.kafka.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SuccessfullAuthMessage {

    private String phone;
    private String message;

}
