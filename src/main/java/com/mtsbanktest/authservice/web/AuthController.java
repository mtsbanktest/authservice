package com.mtsbanktest.authservice.web;

import com.mtsbanktest.authservice.model.request.AuthRequest;
import com.mtsbanktest.authservice.model.request.RefreshTokenRequest;
import com.mtsbanktest.authservice.model.response.AuthResponse;
import com.mtsbanktest.authservice.service.AuthService;
import com.mtsbanktest.authservice.utils.RequestUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("${app.name}/v1")
public class AuthController {
    private final AuthService authService;

    @PostMapping("/auth")
    public ResponseEntity<AuthResponse> authenticateUser(
            @RequestBody AuthRequest authRequest
    ) {
        log.info("authenticating user {}", authRequest.getPhone());
        RequestUtil.validatePhoneNumberFormat(authRequest.getPhone());
        final AuthResponse token = authService.authenticateUser(authRequest);
        return ResponseEntity.ok(token);
    }

    @PostMapping("/token")
    public ResponseEntity<AuthResponse> getNewAccessToken(
            @RequestBody RefreshTokenRequest request
    ) {
        final AuthResponse token = authService.getAccessToken(request.getRefreshToken());
        return ResponseEntity.ok(token);
    }

    @PostMapping("/refresh")
    public ResponseEntity<AuthResponse> getNewRefreshToken(
            @RequestBody RefreshTokenRequest request
    ) {
        final AuthResponse token = authService.refreshToken(request.getRefreshToken());
        return ResponseEntity.ok(token);
    }


}
