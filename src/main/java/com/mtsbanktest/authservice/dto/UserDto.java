package com.mtsbanktest.authservice.dto;

import lombok.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {
    long id;
    String phone;
    String walletId;
    String secretCode;
    LocalDateTime createdDate;
    LocalDateTime updatedDate;
}
