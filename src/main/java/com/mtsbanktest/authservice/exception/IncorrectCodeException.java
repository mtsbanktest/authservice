package com.mtsbanktest.authservice.exception;

public class IncorrectCodeException extends RuntimeException {
    protected static final String MESSAGE = "Неверный код";

    public IncorrectCodeException() {
        super(MESSAGE);
    }

}
