package com.mtsbanktest.authservice.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


public class InvalidPhoneFormatException extends RuntimeException{
    protected static final String MESSAGE = "Номер телефона не соответствует формату";


    public InvalidPhoneFormatException() {
        super(MESSAGE);
    }
}
