package com.mtsbanktest.authservice.exception;

public class UserNotFoundException extends RuntimeException {

    protected static final String MESSAGE = "Пользователь не найден";

    public UserNotFoundException() {
        super(MESSAGE);
    }
}
