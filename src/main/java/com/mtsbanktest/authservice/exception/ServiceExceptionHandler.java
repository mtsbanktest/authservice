package com.mtsbanktest.authservice.exception;

import com.mtsbanktest.authservice.model.response.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@ControllerAdvice
public class ServiceExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleUserNotFoundException(UserNotFoundException exception) {
        log.error("Пользователь не найден: {}", exception.getMessage());
        var body = new ErrorResponse(exception.getMessage());
        return ResponseEntity.status(UNAUTHORIZED).body(body);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handlerIncorrectCodeException(IncorrectCodeException exception) {
        log.error("Неккоректный код для входа: {}", exception.getMessage());
        var body = new ErrorResponse(exception.getMessage());
        return ResponseEntity.status(UNAUTHORIZED).body(body);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleInvalidPhoneFormatException(InvalidPhoneFormatException exception) {
        log.error("Номер телефона не соответствует формату {}", exception.getMessage());
        var body = new ErrorResponse(exception.getMessage());
        return ResponseEntity.status(BAD_REQUEST).body(body);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handlerParseTokenException(ParseTokenException exception) {
        log.error("Невалидный токен: {}", exception.getMessage());
        var body = new ErrorResponse(exception.getMessage());
        return ResponseEntity.status(BAD_REQUEST).body(body);
    }




}
