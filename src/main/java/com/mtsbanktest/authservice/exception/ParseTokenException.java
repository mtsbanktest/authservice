package com.mtsbanktest.authservice.exception;

public class ParseTokenException extends RuntimeException {

    protected static final String MESSAGE = "Невалидный токен";

    public ParseTokenException() {
        super(MESSAGE);
    }

}
