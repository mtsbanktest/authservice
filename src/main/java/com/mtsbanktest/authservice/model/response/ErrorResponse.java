package com.mtsbanktest.authservice.model.response;

public record ErrorResponse(
        String error
) {
}
