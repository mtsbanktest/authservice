package com.mtsbanktest.authservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthResponse {

    private final String type = "Bearer";
    private String accessToken;
    private String refreshToken;
}
